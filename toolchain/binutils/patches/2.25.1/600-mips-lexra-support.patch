--- a/bfd/bfd-in2.h
+++ b/bfd/bfd-in2.h
@@ -1944,6 +1944,7 @@
 #define bfd_mach_mips4100              4100
 #define bfd_mach_mips4111              4111
 #define bfd_mach_mips4120              4120
+#define bfd_mach_mips_lexra            4180
 #define bfd_mach_mips4300              4300
 #define bfd_mach_mips4400              4400
 #define bfd_mach_mips4600              4600
--- a/bfd/cpu-mips.c
+++ b/bfd/cpu-mips.c
@@ -67,6 +67,7 @@
   I_mips4100,
   I_mips4111,
   I_mips4120,
+  I_lexra,
   I_mips4300,
   I_mips4400,
   I_mips4600,
@@ -117,6 +118,7 @@
   N (64, 64, bfd_mach_mips4100, "mips:4100",      FALSE, NN(I_mips4100)),
   N (64, 64, bfd_mach_mips4111, "mips:4111",      FALSE, NN(I_mips4111)),
   N (64, 64, bfd_mach_mips4120, "mips:4120",      FALSE, NN(I_mips4120)),
+  N (32, 32, bfd_mach_mips_lexra, "mips:lexra",   FALSE, NN(I_lexra)),
   N (64, 64, bfd_mach_mips4300, "mips:4300",      FALSE, NN(I_mips4300)),
   N (64, 64, bfd_mach_mips4400, "mips:4400",      FALSE, NN(I_mips4400)),
   N (64, 64, bfd_mach_mips4600, "mips:4600",      FALSE, NN(I_mips4600)),
--- a/gas/config/tc-mips.c
+++ b/gas/config/tc-mips.c
@@ -1350,6 +1350,7 @@
 static int relaxed_micromips_16bit_branch_length (fragS *, asection *, int);
 static int relaxed_micromips_32bit_branch_length (fragS *, asection *, int);
 static void file_mips_check_options (void);
+static inline int lexra_is_insn_swappable (const struct mips_cl_insn *, const struct mips_cl_insn *);
 
 /* Table and functions used to map between CPU/ISA names, and
    ISA levels, and CPU numbers.  */
@@ -2659,6 +2660,18 @@
 #define MIPS16_SPECIAL_REGISTER_NAMES \
     {"$pc",	RTYPE_PC | 0}
 
+#define LEXRA_REGISTER_ALIAS_NAMES \
+    {"$estatus",	RTYPE_GP | 0}, \
+    {"$ecause",		RTYPE_GP | 1}, \
+    {"$intvec",		RTYPE_GP | 2}, \
+    {"$cvstag",		RTYPE_GP | 3},  \
+    {"$bpctl",		RTYPE_GP | 4},  \
+    {"$wmpctl",		RTYPE_GP | 5},  \
+    {"$wmpstatus",	RTYPE_GP | 6},  \
+    {"$wmpvaddr",	RTYPE_GP | 7},  \
+    {"$tlptr",		RTYPE_GP | 8},  \
+    {"$wmpextramask",	RTYPE_GP | 19}
+
 #define MDMX_VECTOR_REGISTER_NAMES \
     /* {"$v0",	RTYPE_VEC | 0},  clash with REG 2 above */ \
     /* {"$v1",	RTYPE_VEC | 1},  clash with REG 3 above */ \
@@ -2724,6 +2737,7 @@
   SYMBOLIC_REGISTER_NAMES,
 
   MIPS16_SPECIAL_REGISTER_NAMES,
+  LEXRA_REGISTER_ALIAS_NAMES,
   MDMX_VECTOR_REGISTER_NAMES,
   R5900_I_NAMES,
   R5900_Q_NAMES,
@@ -4831,6 +4845,9 @@
     case OP_REG_VF:
       return RTYPE_NUM | RTYPE_VF;
 
+    case OP_REG_LXC0:
+      return RTYPE_NUM | RTYPE_CP0;
+
     case OP_REG_R5900_I:
       return RTYPE_R5900_I;
 
@@ -4892,6 +4909,13 @@
 	as_warn (_("condition code register should be 0 or 4 for %s, was %d"),
 		 name, regno);
     }
+
+  if (type == OP_REG_LXC0)
+    {
+      if (regno > 31)
+	as_bad (_("lxc0 register should be smaller than 32 for %s, was %d"),
+		 arg->insn->insn_mo->name, regno);
+    }
 }
 
 /* ARG is a register with symbol value SYMVAL.  Try to interpret it as
@@ -6481,6 +6505,35 @@
   return nops;
 }
 
+static inline int
+lexra_is_insn_swappable (const struct mips_cl_insn *hist,
+                       const struct mips_cl_insn *insn)
+{
+
+  unsigned long pinfo1, pinfo2;
+
+  pinfo1 = hist[0].insn_mo->pinfo;
+  pinfo2 = hist[1].insn_mo->pinfo;
+
+  if (pinfo1 & INSN_LOAD_MEMORY || gpr_write_mask(&hist[0]))
+    {
+      if (gpr_read_mask (insn) & (1 << EXTRACT_OPERAND (mips_opts.micromips, RT, hist[0])))
+        {
+          return 1;
+        }
+    }
+
+  if (pinfo2 & INSN_LOAD_MEMORY || gpr_write_mask(&hist[0]))
+    {
+      if (gpr_read_mask (insn) & (1 << EXTRACT_OPERAND (mips_opts.micromips, RT, hist[1])))
+        {
+          return 1;
+        }
+    }
+
+  return 0;
+}
+
 /* The variable arguments provide NUM_INSNS extra instructions that
    might be added to HIST.  Return the largest number of nops that
    would be needed after the extended sequence, ignoring hazards
@@ -6662,6 +6715,9 @@
   if (nops_for_sequence (2, 0, history + 1, ip, history) > 0)
     return FALSE;
 
+  if (lexra_is_insn_swappable (history, ip) > 0)
+    return FALSE;
+
   /* If the branch reads a register that the previous
      instruction sets, we can not swap.  */
   gpr_read = gpr_read_mask (ip);
@@ -18684,6 +18740,9 @@
      MIPS64R2 rather than MIPS64.  */
   { "xlp",	      0, 0,			ISA_MIPS64R2, CPU_XLR },
 
+  /* Lexra processors */
+  { "lexra",	      0, 0,			ISA_MIPS1,    CPU_LEXRA },
+
   /* End marker */
   { NULL, 0, 0, 0, 0 }
 };
--- a/include/opcode/mips.h
+++ b/include/opcode/mips.h
@@ -464,6 +464,9 @@
   /* Integer registers $vi0-$vi31.  */
   OP_REG_VI,
 
+  /* Lexra lxc0 registers */
+  OP_REG_LXC0,
+
   /* R5900 VU0 registers $I, $Q, $R and $ACC.  */
   OP_REG_R5900_I,
   OP_REG_R5900_Q,
@@ -1190,13 +1193,16 @@
 #undef ISAF
 
 /* Masks used for Chip specific instructions.  */
-#define INSN_CHIP_MASK		  0xc3ff0f20
+#define INSN_CHIP_MASK		  0xc3ff0f60
 
 /* Cavium Networks Octeon instructions.  */
 #define INSN_OCTEON		  0x00000800
 #define INSN_OCTEONP		  0x00000200
 #define INSN_OCTEON2		  0x00000100
 
+/* Lexra instructions */
+#define INSN_LEXRA                0x00000040
+
 /* MIPS R5900 instruction */
 #define INSN_5900                 0x00004000
 
@@ -1288,6 +1294,7 @@
 #define CPU_VR4100	4100
 #define CPU_R4111	4111
 #define CPU_VR4120	4120
+#define CPU_LEXRA	4180		/*  LX4180 */
 #define CPU_R4300	4300
 #define CPU_R4400	4400
 #define CPU_R4600	4600
@@ -1361,6 +1368,9 @@
     case CPU_VR4120:
       return (mask & INSN_4120) != 0;
 
+    case CPU_LEXRA:
+      return (mask & INSN_LEXRA) != 0;
+
     case CPU_VR5400:
       return (mask & INSN_5400) != 0;
 
--- a/opcodes/mips16-opc.c
+++ b/opcodes/mips16-opc.c
@@ -178,6 +178,7 @@
 #define I32	INSN_ISA32
 #define I64	INSN_ISA64
 #define T3	INSN_3900
+#define LX	INSN_LEXRA
 
 const struct mips_opcode mips16_opcodes[] =
 {
@@ -223,6 +224,7 @@
 {"bne",     "x,I,p",	0, (int) M_BNE_I,	INSN_MACRO,		0,		I1,	0,	0 },
 {"bnez",    "x,p",	0x2800, 0xf800,		RD_1,			CBR,		I1,	0,	0 },
 {"break",   "6",	0xe805, 0xf81f,		TRAP,			0,		I1,	0,	0 },
+{"break",   "",		0xe805, 0xffff,		TRAP,			0,		LX,	0,	0 },
 {"bteqz",   "p",	0x6000, 0xff00,		RD_T,			CBR,		I1,	0,	0 },
 {"btnez",   "p",	0x6100, 0xff00,		RD_T,			CBR,		I1,	0,	0 },
 {"cmpi",    "x,U",	0x7000, 0xf800,		RD_1|WR_T,		0,		I1,	0,	0 },
@@ -307,10 +309,18 @@
 {"lw",	    "x,V(P)",	0xb000, 0xf800,		WR_1,			RD_PC,		I1,	0,	0 },
 {"lw",	    "x,V(S)",	0x9000, 0xf800,		WR_1,			RD_SP,		I1,	0,	0 },
 {"lwu",     "y,W(x)",	0xb800, 0xf800,		WR_1|RD_3, 		0,		I3,	0,	0 },
+{"madh",    "x,y",	0xf800, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
+{"madl",    "x,y",	0xf802, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
+{"mazh",    "x,y",	0xf804, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
+{"mazl",    "x,y",	0xf806, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
 {"mfhi",    "x",	0xe810, 0xf8ff,		WR_1|RD_HI,		0,		I1,	0,	0 },
 {"mflo",    "x",	0xe812, 0xf8ff,		WR_1|RD_LO,		0,		I1,	0,	0 },
 {"move",    "y,X",	0x6700, 0xff00,		WR_1|RD_2, 		0,		I1,	0,	0 },
 {"move",    "Y,Z",	0x6500, 0xff00,		WR_1|RD_2,		0,		I1,	0,	0 },
+{"msbh",    "x,y",	0xf810, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
+{"msbl",    "x,y",	0xf812, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
+{"mszh",    "x,y",	0xf814, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
+{"mszl",    "x,y",	0xf816, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		LX,	0,	0 },
 {"mul",     "z,v,y",	0, (int) M_MUL, 	INSN_MACRO,		0,		I1,	0,	0 },
 {"mult",    "x,y",	0xe818, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		I1,	0,	0 },
 {"multu",   "x,y",	0xe819, 0xf81f,		RD_1|RD_2|WR_HI|WR_LO,	0,		I1,	0,	0 },
--- a/opcodes/mips-dis.c
+++ b/opcodes/mips-dis.c
@@ -428,6 +428,15 @@
   "$24",  "$25",  "$26",  "$27",  "$28",  "$29",  "$30",  "$31"
 };
 
+static const char *const mips_cplxc0_names_alias[32] =
+{
+  "estatus",  "ecause",   "intvec",   "cvstag",
+  "bpctl", "wmpctl", "wmpstatus", "wmpvaddr",
+  "tlptr", "$9", "$10", "$11", "$12", "$13", "$14", "$15",
+  "$16", "$17", "$18", "wmpextramask", "$20", "$21", "$22", "$23",
+  "$24", "$25", "$26", "$27", "$28", "$29", "$30", "$31"
+};
+
 struct mips_abi_choice
 {
   const char * name;
@@ -663,6 +672,10 @@
     mips_cp0sel_names_xlr, ARRAY_SIZE (mips_cp0sel_names_xlr),
     mips_cp1_names_mips3264, mips_hwr_names_numeric },
 
+  /* Lexra processors */
+  { "lexra",	1, bfd_mach_mips_lexra, CPU_LEXRA, ISA_MIPS1, 0,
+    mips_cp0_names_numeric, NULL, 0, mips_hwr_names_numeric},
+
   /* This entry, mips16, is here only for ISA/processor selection; do
      not print its name.  */
   { "",		1, bfd_mach_mips16, CPU_MIPS16, ISA_MIPS3, 0,
@@ -1064,6 +1077,10 @@
       info->fprintf_func (info->stream, "$vi%d", regno);
       break;
 
+    case OP_REG_LXC0:
+      info->fprintf_func (info->stream, "%s", mips_cplxc0_names_alias[regno]);
+      break;
+
     case OP_REG_R5900_I:
       info->fprintf_func (info->stream, "$I");
       break;
--- a/opcodes/mips-opc.c
+++ b/opcodes/mips-opc.c
@@ -198,6 +198,7 @@
     case 'i': HINT (16, 0);
     case 'j': SINT (16, 0);
     case 'k': HINT (5, 16);
+    case 'l': REG (5, 11, LXC0);
     case 'o': SINT (16, 0);
     case 'p': BRANCH (16, 0, 2);
     case 'q': HINT (10, 6);
@@ -308,6 +309,7 @@
 #define V1	(INSN_4100 | INSN_4111 | INSN_4120)
 #define T3      INSN_3900
 /* Emotion Engine MIPS r5900. */
+#define LX      INSN_LEXRA
 #define EE      INSN_5900
 #define M1	INSN_10000
 #define SB1     INSN_SB1
@@ -935,7 +937,7 @@
 {"flushid",		"",		0xbc030000, 0xffffffff, 0, 			0,		L1,		0,	0 },
 {"wb",			"o(b)",		0xbc040000, 0xfc1f0000, RD_2|SM,		0,		L1,		0,	0 },
 {"cache",		"k,+j(b)",	0x7c000025, 0xfc00007f, RD_3,			0,		I37,		0,	0 },
-{"cache",		"k,o(b)",	0xbc000000, 0xfc000000, RD_3,           	0,		I3_32|T3,	0,	I37 },
+{"cache",		"k,o(b)",	0xbc000000, 0xfc000000, RD_3,           	0,		I3_32|T3|LX,	0,	I37 },
 {"cache",		"k,A(b)",	0,    (int) M_CACHE_AB, INSN_MACRO,		0,		I3_32|T3,	0,	0 },
 {"ceil.l.d",		"D,S",		0x4620000a, 0xffff003f, WR_1|RD_2|FP_D,		0,		I3_33,		0,	0 },
 {"ceil.l.s",		"D,S",		0x4600000a, 0xffff003f, WR_1|RD_2|FP_S|FP_D,	0,		I3_33,		0,	0 },
@@ -997,7 +999,7 @@
 /* dctr and dctw are used on the r5000.  */
 {"dctr",		"o(b)",	 	0xbc050000, 0xfc1f0000, RD_2,			0,		I3,		0,	0 },
 {"dctw",		"o(b)",		0xbc090000, 0xfc1f0000, RD_2,			0,		I3,		0,	0 },
-{"deret",		"",		0x4200001f, 0xffffffff, NODS, 			0,		I32|G2,		0,	0 },
+{"deret",		"",		0x4200001f, 0xffffffff, NODS, 			0,		I32|G2|LX,		0,	0 },
 {"dext",		"t,r,+A,+H",	0x7c000003, 0xfc00003f, WR_1|RD_2,    		0,		I65,		0,	0 },
 {"dext",		"t,r,+A,+G",	0x7c000001, 0xfc00003f, WR_1|RD_2,    		0,		I65,		0,	0 }, /* dextm */
 {"dext",		"t,r,+E,+H",	0x7c000002, 0xfc00003f, WR_1|RD_2,    		0,		I65,		0,	0 }, /* dextu */
@@ -1313,8 +1315,8 @@
 {"maccu",		"d,s,t",	0x00000068, 0xfc0007ff,	WR_1|RD_2|RD_3|WR_HILO,	     0,		N412,		0,	0 },
 {"maccu",		"d,s,t",	0x00000159, 0xfc0007ff, WR_1|RD_2|RD_3|WR_HILO,	     0,		N5,		0,	0 },
 {"maccus",		"d,s,t",	0x00000468, 0xfc0007ff,	WR_1|RD_2|RD_3|WR_HILO,	     0,		N412,		0,	0 },
-{"mad",			"s,t",		0x70000000, 0xfc00ffff, RD_1|RD_2|MOD_HILO,	     0,		P3,		0,	0 },
-{"madu",		"s,t",		0x70000001, 0xfc00ffff, RD_1|RD_2|MOD_HILO,	     0,		P3,		0,	0 },
+{"mad",			"s,t",		0x70000000, 0xfc00ffff, RD_1|RD_2|MOD_HILO,	     0,		P3|LX,		0,	0 },
+{"madu",		"s,t",		0x70000001, 0xfc00ffff, RD_1|RD_2|MOD_HILO,	     0,		P3|LX,		0,	0 },
 {"madd.d",		"D,R,S,T",	0x4c000021, 0xfc00003f, WR_1|RD_2|RD_3|RD_4|FP_D,    0,		I4_33,		0,	I37 },
 {"madd.d",		"D,S,T",	0x46200018, 0xffe0003f,	WR_1|RD_2|RD_3|FP_D,	     0,		IL2E,		0,	0 },
 {"madd.d",		"D,S,T",	0x72200018, 0xffe0003f,	WR_1|RD_2|RD_3|FP_D,	     0,		IL2F,		0,	0 },
@@ -1413,7 +1415,7 @@
 {"movf.l",		"X,Y,N",	0x46a00011, 0xffe3003f, WR_1|RD_2|RD_CC|FP_D,	0,		SB1,		MX,	0 },
 {"movf.s",		"D,S,N",	0x46000011, 0xffe3003f, WR_1|RD_2|RD_CC|FP_S,   0,		I4_32,		0,	I37 },
 {"movf.ps",		"D,S,N",	0x46c00011, 0xffe3003f, WR_1|RD_2|RD_CC|FP_D,	0,		I5_33,		0,	I37 },
-{"movn",		"d,v,t",	0x0000000b, 0xfc0007ff, WR_1|RD_2|RD_3, 	0,		I4_32|IL2E|IL2F|EE, 0,	I37 },
+{"movn",		"d,v,t",	0x0000000b, 0xfc0007ff, WR_1|RD_2|RD_3, 	0,		I4_32|IL2E|IL2F|EE|LX, 0,	I37 },
 {"movnz",		"d,v,t",	0x0000000b, 0xfc0007ff, WR_1|RD_2|RD_3, 	0,		IL2E|IL2F|IL3A,	0,	0 },
 {"ffc",			"d,v",		0x0000000b, 0xfc1f07ff,	WR_1|RD_2,		0,		L1,		0,	0 },
 {"movn.d",		"D,S,t",	0x46200013, 0xffe0003f, WR_1|RD_2|RD_3|FP_D,    0,		I4_32,		0,	I37 },
@@ -1427,7 +1429,7 @@
 {"movt.l",		"X,Y,N",	0x46a10011, 0xffe3003f, WR_1|RD_2|RD_CC|FP_D,   0,		SB1,		MX,	0 },
 {"movt.s",		"D,S,N",	0x46010011, 0xffe3003f, WR_1|RD_2|RD_CC|FP_S,   0,		I4_32,		0,	I37 },
 {"movt.ps",		"D,S,N",	0x46c10011, 0xffe3003f, WR_1|RD_2|RD_CC|FP_D,	0,		I5_33,		0,	I37 },
-{"movz",		"d,v,t",	0x0000000a, 0xfc0007ff, WR_1|RD_2|RD_3, 	0,		I4_32|IL2E|IL2F|EE, 0,	I37 },
+{"movz",		"d,v,t",	0x0000000a, 0xfc0007ff, WR_1|RD_2|RD_3, 	0,		I4_32|IL2E|IL2F|EE|LX, 0,	I37 },
 {"ffs",			"d,v",		0x0000000a, 0xfc1f07ff,	WR_1|RD_2,		0,		L1,		0,	0 },
 {"movz.d",		"D,S,t",	0x46200012, 0xffe0003f, WR_1|RD_2|RD_3|FP_D,    0,		I4_32,		0,	I37 },
 {"movz.l",		"D,S,t",	0x46a00012, 0xffe0003f, WR_1|RD_2|RD_3|FP_D,    0,		SB1,		MX,	0 },
@@ -1456,11 +1458,11 @@
 {"msub.ps",		"D,S,T",	0x45600019, 0xffe0003f,	WR_1|RD_2|RD_3|FP_D,	0,		IL2E,		0,	0 },
 {"msub.ps",		"D,S,T",	0x72c00019, 0xffe0003f,	WR_1|RD_2|RD_3|FP_D,	0,		IL2F,		0,	0 },
 {"msub",		"s,t",		0x0000001e, 0xfc00ffff, RD_1|RD_2|WR_HILO,	0,		L1,		0,	0 },
-{"msub",		"s,t",		0x70000004, 0xfc00ffff, RD_1|RD_2|MOD_HILO,     0,		I32|N55,	0,	I37 },
+{"msub",		"s,t",		0x70000004, 0xfc00ffff, RD_1|RD_2|MOD_HILO,     0,		I32|N55|LX,	0,	I37 },
 {"msub",		"7,s,t",	0x70000004, 0xfc00e7ff, RD_2|RD_3|MOD_a,        0,              0,		D32,	0 },
 {"msuba.s",		"S,T",		0x4600001f, 0xffe007ff,	RD_1|RD_2|FP_S,		0,		EE,		0,	0 },
 {"msubu",		"s,t",		0x0000001f, 0xfc00ffff, RD_1|RD_2|WR_HILO,	0,		L1,		0,	0 },
-{"msubu",		"s,t",		0x70000005, 0xfc00ffff, RD_1|RD_2|MOD_HILO,     0,		I32|N55,	0,	I37 },
+{"msubu",		"s,t",		0x70000005, 0xfc00ffff, RD_1|RD_2|MOD_HILO,     0,		I32|N55|LX,	0,	I37 },
 {"msubu",		"7,s,t",	0x70000005, 0xfc00e7ff, RD_2|RD_3|MOD_a,        0,              0,		D32,	0 },
 {"mtbpc",		"t",		0x4080c000, 0xffe0ffff,	RD_1|WR_C0|CM,		0,		EE,		0,	0 },
 {"mtdab",		"t",		0x4080c004, 0xffe0ffff,	RD_1|WR_C0|CM,		0,		EE,		0,	0 },
@@ -1794,9 +1796,9 @@
 {"sdbbp",		"c",		0x0000000e, 0xfc00ffff,	TRAP,			0,		G2,		0,	0 },
 {"sdbbp",		"c,q",		0x0000000e, 0xfc00003f,	TRAP,			0,		G2,		0,	0 },
 {"sdbbp",		"",		0x0000000e, 0xffffffff, TRAP,           	0,		I37,		0,	0 },
-{"sdbbp",		"",		0x7000003f, 0xffffffff, TRAP,           	0,		I32,		0,	I37 },
+{"sdbbp",		"",		0x7000003f, 0xffffffff, TRAP,           	0,		I32|LX,		0,	I37 },
 {"sdbbp",		"B",		0x0000000e, 0xfc00003f, TRAP,           	0,		I37,		0,	0 },
-{"sdbbp",		"B",		0x7000003f, 0xfc00003f, TRAP,           	0,		I32,		0,	I37 },
+{"sdbbp",		"B",		0x7000003f, 0xfc00003f, TRAP,           	0,		I32|LX,		0,	I37 },
 {"sdc1",		"T,o(b)",	0xf4000000, 0xfc000000, RD_1|RD_3|SM|FP_D,	0,		I2,		0,	SF },
 {"sdc1",		"E,o(b)",	0xf4000000, 0xfc000000, RD_1|RD_3|SM|FP_D,	0,		I2,		0,	SF },
 {"sdc1",		"T,A(b)",	0,    (int) M_SDC1_AB,	INSN_MACRO,		INSN2_M_FP_D,	I2,		0,	SF },
@@ -3143,26 +3145,32 @@
 {"udi0",		"s,t,+2",	0x70000010, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi0",		"s,+3",		0x70000010, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi0",		"+4",		0x70000010, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
+{"udi0",		"d,v,t",	0x00000038, 0xfc0007ff, UDI,			0,		LX,		0,	0 },
 {"udi1",		"s,t,d,+1",	0x70000011, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi1",		"s,t,+2",	0x70000011, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi1",		"s,+3",		0x70000011, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi1",		"+4",		0x70000011, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
+{"udi1",		"d,v,t",	0x0000003a, 0xfc0007ff, UDI,			0,		LX,		0,	0 },
 {"udi2",		"s,t,d,+1",	0x70000012, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi2",		"s,t,+2",	0x70000012, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi2",		"s,+3",		0x70000012, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi2",		"+4",		0x70000012, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
+{"udi2",		"d,v,t",	0x0000003b, 0xfc0007ff, UDI,			0,		LX,		0,	0 },
 {"udi3",		"s,t,d,+1",	0x70000013, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi3",		"s,t,+2",	0x70000013, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi3",		"s,+3",		0x70000013, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi3",		"+4",		0x70000013, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
+{"udi3",		"d,v,t",	0x0000003c, 0xfc0007ff, UDI,			0,		LX,		0,	0 },
 {"udi4",		"s,t,d,+1",	0x70000014, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi4",		"s,t,+2",	0x70000014, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi4",		"s,+3",		0x70000014, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi4",		"+4",		0x70000014, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
+{"udi4",		"d,v,t",	0x0000003e, 0xfc0007ff, UDI,			0,		LX,		0,	0 },
 {"udi5",		"s,t,d,+1",	0x70000015, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi5",		"s,t,+2",	0x70000015, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi5",		"s,+3",		0x70000015, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi5",		"+4",		0x70000015, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
+{"udi5",		"d,v,t",	0x0000003f, 0xfc0007ff, UDI,			0,		LX,		0,	0 },
 {"udi6",		"s,t,d,+1",	0x70000016, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi6",		"s,t,+2",	0x70000016, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
 {"udi6",		"s,+3",		0x70000016, 0xfc00003f,	UDI,			0,		I33,		0,	0 },
@@ -3336,6 +3344,24 @@
 {"cop3",		"C",		0,    (int) M_COP3,	INSN_MACRO,		0,		I1,		0,	IOCT|IOCTP|IOCT2 },
 /* RFE conflicts with the new Virt spec instruction tlbgp. */
 {"rfe",			"",		0x42000010, 0xffffffff,	0,			0,		I1|T3,		0,	0 },
+
+/* Lexra Coprocessor 0 operations */
+{"mflxc0",		"t,l",		0x40600000, 0xffe007ff, WR_1|RD_C0|LC,		0,		LX,		0,	0 },
+{"mtlxc0",		"t,l",		0x40e00000, 0xffe007ff, RD_1|WR_C0|CM,		0,		LX,		0,	0 },
+/* MAC-DIV */
+{"sleep",		"",		0x42000038, 0xffffffff, 0,			0,		LX,		0,	0 },
+{"madh",		"s,t",		0xf0000000, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"madl",		"s,t",		0xf0000002, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"mazh",		"s,t",		0xf0000004, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"mazl",		"s,t",		0xf0000006, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"msbh",		"s,t",		0xf0000010, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"msbl",		"s,t",		0xf0000012, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"mszh",		"s,t",		0xf0000014, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"mszl",		"s,t",		0xf0000016, 0xfc00ffff, RD_1|RD_2,		0,		LX,		0,	0 },
+{"udi0i",		"t,r,j",	0x60000000, 0xfc000000, WR_1|RD_2,		0,		LX,		0,	0 },
+{"udi1i",		"t,r,j",	0x64000000, 0xfc000000, WR_1|RD_2,		0,		LX,		0,	0 },
+{"udi2i",		"t,r,j",	0x68000000, 0xfc000000, WR_1|RD_2,		0,		LX,		0,	0 },
+{"udi3i",		"t,r,j",	0x6c000000, 0xfc000000, WR_1|RD_2,		0,		LX,		0,	0 },
 };
 
 #define MIPS_NUM_OPCODES \
